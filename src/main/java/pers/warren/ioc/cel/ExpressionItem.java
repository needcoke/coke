package pers.warren.ioc.cel;

/**
 * CEL解析表达式项接口
 *
 * @author warren
 * @since 1.0.3
 */
public interface ExpressionItem {

    String STR_MODE = "$(str::";

    String BEAN_MODE = "$(bean";

    String BEAN_MODE_M = "$(bean-m::";

    String BEAN_MODE_F = "$(bean-f::";

    String BEAN_MODE_E = "$(bean::";

    String ENV_MODE = "$(env::";

    String JAVA_MODE = "$(j::";

    /**
     * 将表达式string转为表达式项
     *
     * @since 1.0.3
     */
    static ExpressionItem getExpressionItem(String expressionString) {
        if (expressionString.startsWith("$(") && expressionString.endsWith(")")) {
            if (expressionString.startsWith(STR_MODE)) {
                return new CELItem(expressionString);
            } else if (expressionString.startsWith(BEAN_MODE_M)) {
                return new CELBeanItem(expressionString);
            } else if (expressionString.startsWith(BEAN_MODE_F)) {
                return new CELBeanItem(expressionString);
            } else if (expressionString.startsWith(BEAN_MODE_E)) {
                return new CELBeanItem(expressionString);
            } else if (expressionString.startsWith(ENV_MODE)) {
                return new CELEnvItem(expressionString);
            } else if (expressionString.startsWith(JAVA_MODE)) {
                return new CELItem(expressionString);
            } else {
                return new SimpleItem(expressionString);
            }
        }
        return new SimpleItem(expressionString);
    }

    /**
     * 获取表达式项对应的值
     *
     * @since 1.0.3
     */
    String getValue();
}
