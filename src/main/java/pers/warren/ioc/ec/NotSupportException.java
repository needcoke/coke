package pers.warren.ioc.ec;

/**
 * 不支持的异常
 *
 * @author warren
 * @since 1.0.3
 */
public class NotSupportException extends RuntimeException {
    
    public NotSupportException(String message) {
        super(message);
    }
}
