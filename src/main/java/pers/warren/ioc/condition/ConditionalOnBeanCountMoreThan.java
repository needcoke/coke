package pers.warren.ioc.condition;

import java.lang.annotation.*;
/**
 * 目前该注解仅对@Bean定义的bean生效
 * <p>只有当指定类型的bean超过</p>
 *
 * @author warren
 * @since 1.0.3
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ConditionalOnBeanCountMoreThan {

    /**
     * 数量
     *
     * <p></p>
     */
    int count() default 0;

    /**
     * 需要作为条件的类的Class对象数组
     */
    Class<?>[] value() default {};
}
