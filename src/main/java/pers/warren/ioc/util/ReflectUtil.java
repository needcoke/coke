package pers.warren.ioc.util;

import cn.hutool.core.util.StrUtil;
import javassist.*;
import javassist.bytecode.*;
import lombok.experimental.UtilityClass;
import pers.warren.ioc.annotation.Param;
import pers.warren.ioc.ec.NotSupportException;

import java.lang.reflect.*;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * coke反射工具类
 *
 * @author warren
 * @since 1.0.3
 */
@UtilityClass
public class ReflectUtil {

    /**
     * 获取方法的参数名称
     *
     * @param method 方法
     *
     *               <p>抽象方法(包括接口非默认方法和抽象类的abstract方法,无法读到实际名称,会显示arg0，args1 .... 可以通过注解@Param赋予名称,@Param中value指定的名称会被优先读出来。</p>
     *               <p>对于一般类的方法，能直接获取到名称，当然也能通过@Param更高优先级的指定名称</p>
     * @since 1.0.3
     */
    public String[] getParameterNames(final Method method) {
        if (Modifier.isInterface(method.getDeclaringClass().getModifiers()) ||
            Modifier.isAbstract(method.getDeclaringClass().getModifiers())) {
            return getAbstractMethodParameterNames(method);
        }
        List<String> paramNames = new ArrayList<>();
        ClassPool pool = ClassPool.getDefault();
        try {
            CtClass ctClass = pool.getCtClass(method.getDeclaringClass().getName());
            CtClass[] ctClasses = changeArray(method.getParameterTypes());
            CtMethod ctMethod = ctClass.getDeclaredMethod(method.getName(), ctClasses);
            // 使用javassist的反射方法的参数名
            javassist.bytecode.MethodInfo methodInfo = ctMethod.getMethodInfo();
            CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
            LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
            String[] annotationNames = getAnnotationNames(method.getParameters(),true);
            if (attr != null) {
                int len = ctMethod.getParameterTypes().length;
                // 非静态的成员函数的第一个参数是this
                int pos = Modifier.isStatic(ctMethod.getModifiers()) ? 0 : 1;
                for (int i = 0; i < len; i++) {
                    if(StrUtil.isNotEmpty(annotationNames[paramNames.size()])) {
                        paramNames.add(annotationNames[paramNames.size()]);
                    }
                    else {
                        paramNames.add(attr.variableName(i + pos));
                    }
                }

            }
            String[] arr = new String[paramNames.size()];
            return paramNames.toArray(arr);
        } catch (NotFoundException e) {
            String msg = method.getDeclaringClass().getName() + "#" + method.getName();
            throw new RuntimeException("get method param error method = " + msg, e);
        }
    }

    public static String[] getAbstractMethodParameterNames(final Method method) {
        Parameter[] parameters = method.getParameters();
        return getAnnotationNames(parameters,false);
    }



    public static List<String> getParameterNames(Constructor<?> constructor) {
        List<String> paramNames = new ArrayList<>();
        ClassPool pool = ClassPool.getDefault();
        try {
            Class<?> declaringClass = constructor.getDeclaringClass();
            CtClass ctClass = pool.getCtClass(declaringClass.getName());
            CtConstructor[] constructors = ctClass.getConstructors();
            Class<?>[] pts = constructor.getParameterTypes();
            CtConstructor ctc = null;
            for (CtConstructor ctConstructor : constructors) {
                if (ctConstructor.isConstructor() && ctConstructor.getModifiers() == constructor.getModifiers()) {
                    CtClass[] parameterTypes = ctConstructor.getParameterTypes();
                    if (null != parameterTypes && pts.length == parameterTypes.length) {
                        boolean match = true;
                        for (int i = 0; i < parameterTypes.length; i++) {
                            if (!parameterTypes[i].getName().equals(pts[i].getName())) {
                                match = false;
                            }
                        }
                        if (match) {
                            ctc = ctConstructor;
                            break;
                        }
                    }
                }
            }
            // 使用javassist的反射方法的参数名
            javassist.bytecode.MethodInfo methodInfo = ctc.getMethodInfo();
            CodeAttribute codeAttribute = methodInfo.getCodeAttribute();
            String[] annotationNames =  getAnnotationNames(constructor.getParameters(),true);
            LocalVariableAttribute attr = (LocalVariableAttribute) codeAttribute.getAttribute(LocalVariableAttribute.tag);
            if (attr != null) {
                int len = ctc.getParameterTypes().length;
                // 非静态的成员函数的第一个参数是this
                int pos = Modifier.isStatic(ctc.getModifiers()) ? 0 : 1;
                for (int i = 0; i < len; i++) {
                    if(StrUtil.isNotEmpty(annotationNames[paramNames.size()])) {
                        paramNames.add(annotationNames[paramNames.size()]);
                    } else {
                        paramNames.add(attr.variableName(i + pos));
                    }
                }

            }

            return paramNames;
        } catch (NotFoundException e) {
            String msg = constructor.getDeclaringClass().getName() + "#" + constructor.getName();
            throw new RuntimeException("get constructor param error method = " + msg, e);
        }
    }

    public boolean containsAnnotation(Method method, Class annotationClz) {
        if (null != method.getAnnotation(annotationClz)) {
            return true;
        }
        return false;
    }

    public boolean containsAnnotation(Parameter parameter, Class annotationClz) {
        if (null != parameter.getAnnotation(annotationClz)) {
            return true;
        }
        return false;
    }

    public boolean containsAnnotation(Class<?> clz, Class annotationClz) {
        if (null != clz.getAnnotation(annotationClz)) {
            return true;
        }
        return false;
    }

    private CtClass[] changeArray(Class[] array) throws NotFoundException {
        ClassPool pool = ClassPool.getDefault();
        CtClass[] clzArr = new CtClass[array.length];
        for (int i = 0; i < array.length; i++) {
            clzArr[i] = pool.get(array[i].getName());
        }
        return clzArr;
    }

    public Class<?> mainClz;

    public Class<?> customEntryClass;

    /* 从堆栈信息推测主类 */
    public static Class<?> deduceMainApplicationClass() {
        if (null != mainClz) {
            return mainClz;
        }
        try {
            StackTraceElement[] stackTrace = new RuntimeException().getStackTrace();
            long count = Arrays.stream(stackTrace).filter(e -> e.getClassName().equals("org.junit.runner.JUnitCore")).count();
            for (StackTraceElement stackTraceElement : stackTrace) {
                if ("main".equals(stackTraceElement.getMethodName()) && count == 0) {
                    mainClz = Class.forName(stackTraceElement.getClassName());
                    return mainClz;
                }
            }
            return customEntryClass;
        } catch (ClassNotFoundException ex) {
        }
        return null;
    }

    /**
     * 是否是实现类
     *
     * @auther warren
     *
     * @since  1.0.3
     *
     * */
    public boolean isRealClass(Class<?> clz) {
        if (clz.isInterface() || clz.isAnnotation() || clz.isEnum() || clz.isAnonymousClass() || clz.isLocalClass()) {
            return false;
        }
        return true;
    }


    /**
     * 反射设置属性
     */
    public void setFieldAttr(Class<?> clz , String fieldName , Object value){
        try {
            Field field = clz.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(clz, value);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String[] getAnnotationNames(Parameter[] parameters,boolean noAnnotationIsNull){
        if(null == parameters){
            return new String[0];
        }

        return Arrays.stream(parameters).map(parameter -> {
            Param param = parameter.getAnnotation(Param.class);
            if (null != param && StrUtil.isNotEmpty(param.value())) {
                return param.value();
            }
            return noAnnotationIsNull ? null : parameter.getName();
        }).collect(Collectors.toList()).toArray(new String[parameters.length]);
    }


}
