package pers.warren.ioc.annotation;

import java.lang.annotation.*;

/**
 * 该参数的意义在于通过反射很难正确的获取参数的名称，因此通过该注解来指定参数的名称
 * <p>
 * {@link pers.warren.ioc.util.ReflectUtil} 获取参数名称的方法,天然支持@Param注解
 *
 * @author warren
 * @since 1.0.3
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Param {

    /**
     * 参数名称
     *
     * @author warren
     * @since 1.0.3
     */
    String value() default "";
}
