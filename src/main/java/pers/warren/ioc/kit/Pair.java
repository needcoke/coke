package pers.warren.ioc.kit;

import lombok.Data;

@Data
public class Pair <K,V>{

    private K key;

    private V value;

    public Pair() {
    }

    public Pair(K key, V value) {
        this.key = key;
        this.value = value;
    }
}
