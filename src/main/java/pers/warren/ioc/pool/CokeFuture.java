package pers.warren.ioc.pool;

import java.util.concurrent.Future;

/**
 * coke异步future增强
 *
 * @author warren
 * @since 1.0.3
 */
public interface CokeFuture<T,R> extends Future<T> {

    /**
     * 异步获取属性
     *
     * @since 1.0.3
     */
    CokeFuture<R,?> getAttribute();





}
