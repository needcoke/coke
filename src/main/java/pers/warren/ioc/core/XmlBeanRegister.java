package pers.warren.ioc.core;

/**
 * XML Bean注册器
 *
 * @author warren
 * @since 1.0.4
 */
public abstract class XmlBeanRegister implements BeanRegister {

    /**
     * coke 1.0.4将会支持在coke.xml中配置bean
     */
}
