package pers.warren.ioc.cel;


import cn.hutool.core.collection.CollectionUtil;
import pers.warren.ioc.core.BeanDefinition;
import pers.warren.ioc.inject.InjectField;
import pers.warren.ioc.kit.Pair;

import java.util.*;
import java.util.function.Consumer;

public class CELRunTimeCache {

    private static final Set<String> envKeys = new HashSet<>();

    private static final Set<String> beanKeys = new HashSet<>();

    public static final ExpressionParser celParser = new CokeExpressionParser();

    public static final Map<InjectField, BeanDefinition> beanInjectMap = new HashMap<>();


    public static void addEnvKey(String envKey) {
        envKeys.add(envKey);
    }

    public static void addBeanKey(String beanKey) {
        beanKeys.add(beanKey);
    }

    public static void eachEnvKey(Consumer<String> consumer) {
        envKeys.forEach(consumer::accept);
    }

    public static void eachBeanKey(Consumer<Pair<InjectField,BeanDefinition>> consumer) {
        Set<InjectField> injectFields = beanInjectMap.keySet();
        for (InjectField injectField : injectFields) {
            BeanDefinition beanDefinition = beanInjectMap.get(injectField);
            Pair<InjectField,BeanDefinition> pair = new Pair<>(injectField,beanDefinition);
            consumer.accept(pair);
        }
    }

    public static boolean containsBeanKey(String beanKey) {
        return beanKeys.contains(beanKey);
    }

    public static void addBeanInject(InjectField injectField, BeanDefinition beanDefinition) {
        beanInjectMap.put(injectField, beanDefinition);
    }

}
